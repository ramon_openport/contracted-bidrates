<?php include("inc.header.php"); ?>

<title>Contracted Bid Rates</title>



</head>

<body class="opfw contracted-rates contracted-rates-shipper">
<div class="container-fluid">

<!--PAGE HEADER-->
<section class="row align-items-center">
	<div class="col-auto mr-auto">
		<h1 class="page-title">Contracted Rates Work Area</h1>
	</div>
	<div class="col-auto mr-auto">
		<ul class="nav">
		<li><a href="initial.php">Work Area</a></li>
		<li><a href="submitted.php">Submitted</a></li>
		</ul>
	</div>
	<div class="col-auto">
		<a class="btn btn-primary" href="#"> Submit Selected to Marketplace</a>
	</div>
</section>


<!--OP-BOX BEGIN-->
<section class="row ">
	<div class="col">
		<article class="container-fluid op-box">
		
			<!--HEADER-->
			<div class="row op-box-header align-items-center">
				<div class="col-auto">
					<input type="checkbox" class="">
				</div>
				<div class="col">
					<h1><a href="#" class="x-editable date" data-type="combodate"  data-value="2017-09-15"></a> &mdash; <a href="#" class="x-editable date" data-type="combodate"   data-value="2018-02-15"></a> </h1>
				</div>
				<div class="col-auto  ">
					<a class="btn " href="#"> Submit to Marketplace</a>
				</div>
			</div>

			<!--Lane & COMMODITY PAIR TABLE-->
			<div class="row ">
				<div class="col-12 ">
					<table class="table table-responsive table-bordered " >
					
						<thead>
							<tr>
								<th><input type="checkbox" class=""></th>
								<th colspan="2">Controls</th>
								<th>Origin</th>
								<th>Destination</th>
								<th>Commodity</th>
								<th>Equipment </th>

								<th>Save</th>
							</tr>
						</thead>	
						
						<tbody><!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td ><input type="checkbox" class=""></td>
								<td ><i class="material-icons">content_copy</i></td>
								<td ><i class="material-icons">delete</i></td>
								<td >
									<a href="#" class="x-editable text">Cebu</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td >
									<a href="#" class="x-editable text">Mactan</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td ><a href="#" class="x-editable text">Enter Commodity</a></td>
								<td >
									<label class="checkbox-inline"><input type="checkbox" value=""> 150 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 20 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
								</td>
								<td ><i class="material-icons">save</i></td>
							</tr>
						</tbody><!--Lane & COMMODITY PAIR END-->
						
						<tbody><!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td ><input type="checkbox" class=""></td>
								<td ><i class="material-icons">content_copy</i></td>
								<td ><i class="material-icons">delete</i></td>
								<td >
									<a href="#" class="x-editable text">Lahug</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td >
									<a href="#" class="x-editable text">Oslob</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td ><a href="#" class="x-editable text">Enter Commodity</a></td>
								<td >
									<label class="checkbox-inline"><input type="checkbox" value=""> 150 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 20 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
								</td>
								<td ><i class="material-icons">save</i></td>
							</tr>
						</tbody><!--Lane & COMMODITY PAIR END-->
						
						<tbody><!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td ><input type="checkbox" class=""></td>
								<td ><i class="material-icons">content_copy</i></td>
								<td ><i class="material-icons">delete</i></td>
								<td >
									<a href="#" class="x-editable text">Cebu</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td >
									<a href="#" class="x-editable text">Mactan</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td ><a href="#" class="x-editable text">Enter Commodity</a></td>
								<td >
									<label class="checkbox-inline"><input type="checkbox" value=""> 150 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 20 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
								</td>
								<td ><i class="material-icons">save</i></td>
							</tr>
						</tbody><!--Lane & COMMODITY PAIR END-->
						
					</table>
				</div>
			</div>

			
			<!--Lane & COMMODITY CONTROLS-->
			<div class="row op-box-footer">
				<div class="col-auto">
					<a class="btn btn-outline-primary" href="#">Clone Selected Lanes</a>
				</div>
				<div class="col-auto mr-auto">
					<a class="btn btn-outline-danger " href="#">Delete Selected Lanes</a>
				</div>
				<div class="col-auto">
					<a class="btn btn-outline-primary" href="#">Add New Lane &amp; Commodity Pair</a>
				</div>
			</div>
			
		</article>	
	</div>
</section>

<!--OP-BOX BEGIN-->
<section class="row ">
	<div class="col">
		<article class="container-fluid op-box">
		
			<!--HEADER-->
			<div class="row op-box-header align-items-center">
				<div class="col-auto">
					<input type="checkbox" class="">
				</div>
				<div class="col">
					<h1><a href="#" class="x-editable date" data-type="combodate"  >Enter Start Date</a> &mdash; <a href="#" class="x-editable date" data-type="combodate" >Enter End Date</a> </h1>
				</div>
				<div class="col-auto  ">
					<a class="btn " href="#"> Submit to Marketplace</a>
				</div>
			</div>

			<!--Lane & COMMODITY PAIR TABLE-->
			<div class="row ">
				<div class="col-12 ">
					<table class="table table-responsive table-bordered " >
					
						<thead>
							<tr>
								<th><input type="checkbox" class=""></th>
								<th colspan="2">Controls</th>
								<th>Origin</th>
								<th>Destination</th>
								<th>Commodity</th>
								<th>Equipment </th>
								<th>Save</th>
							</tr>
						</thead>	
						
						<tbody><!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td ><input type="checkbox" class=""></td>
								<td ><i class="material-icons">content_copy</i></td>
								<td ><i class="material-icons">delete</i></td>
								<td >
									<a href="#" class="x-editable text">Cebu</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td >
									<a href="#" class="x-editable text">Mactan</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td ><a href="#" class="x-editable text">Enter Commodity</a></td>
								<td >
									<label class="checkbox-inline"><input type="checkbox" value=""> 150 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 20 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
								</td>
								<td ><i class="material-icons">save</i></td>
							</tr>
						</tbody><!--Lane & COMMODITY PAIR END-->
						
						<tbody><!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td ><input type="checkbox" class=""></td>
								<td ><i class="material-icons">content_copy</i></td>
								<td ><i class="material-icons">delete</i></td>
								<td >
									<a href="#" class="x-editable text">Lahug</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td >
									<a href="#" class="x-editable text">Oslob</a>, 
									<a href="#" class="x-editable text">Provvince/State</a>, 
									<a href="#" class="x-editable text">Postal Code (ZIP)</a>, 
									<a href="#" class="x-editable text">Country</a>
								</td>
								<td ><a href="#" class="x-editable text">Enter Commodity</a></td>
								<td >
									<label class="checkbox-inline"><input type="checkbox" value=""> 150 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 20 FT</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
									<label class="checkbox-inline"><input type="checkbox" value=""> 6 W</label>
								</td>
								<td ><i class="material-icons">save</i></td>
							</tr>
						</tbody><!--Lane & COMMODITY PAIR END-->
						

						
					</table>
				</div>
			</div>

			
			<!--Lane & COMMODITY CONTROLS-->
			<div class="row op-box-footer">
				<div class="col-auto">
					<a class="btn btn-outline-primary" href="#">Clone Selected Lanes</a>
				</div>
				<div class="col-auto mr-auto">
					<a class="btn btn-outline-danger " href="#">Delete Selected Lanes</a>
				</div>
				<div class="col-auto">
					<a class="btn btn-outline-primary" href="#">Add New Lane &amp; Commodity Pair</a>
				</div>
			</div>
			
		</article>	
	</div>
</section>

<section class="row">
	<div class="col-auto ml-auto">
		<a class="btn btn-primary">Add New Contract Period</a>
	</div>
</section>
<!--OP-BOX END-->


</div>


<?php include("inc.footer.php"); ?>

