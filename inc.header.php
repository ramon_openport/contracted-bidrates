<!DOCTYPE html>
<html >
<head>

<meta charset="UTF-8">
<meta name="theme-color" content="#3E8FDE" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="shortcut icon" href="favicon.png" /> 

<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Material+Icons|Work+Sans:300,400,600,700|Roboto+Mono:400,500,700">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0-beta/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/x-editable@1.5.1/dist/bootstrap3-editable/css/bootstrap-editable.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/x-editable@1.5.1/dist/inputs-ext/address/address.css">


<script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.6/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/x-editable@1.5.1/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment@2.18.1/moment.min.js"></script>

<link rel="stylesheet" href="inc.scss.php/style.scss">
