<?php include("inc.header.php"); ?>


<title>Contracted Bid Rates</title>



</head>

<body class="opfw contracted-rates contracted-rates-shipper">
<div class="container-fluid">

<!--PAGE HEADER-->
<section class="row align-items-center">
	<div class="col-auto mr-auto">
		<h1 class="page-title">Contracted Rates Work Area</h1>
	</div>
	<div class="col-auto mr-auto">
		<ul class="nav">
		<li><a href="initial.php">Work Area</a></li>
		<li><a href="submitted.php">Submitted</a></li>
		</ul>
	</div>
	<div class="col-auto">
		<a class="btn btn-primary" href="#"> Withdraw Tender for Selected Contract Periods</a>
	</div>
</section>


<!--OP-BOX BEGIN-->
<section class="row ">
	<div class="col">
		<article class="container-fluid op-box">
		
			<!--HEADER-->
			<div class="row op-box-header align-items-center">
				<div class="col-auto">
					<input type="checkbox" class="">
				</div>
				<div class="col-auto mr-auto">
					<h1>2018/05/15 — 2019/08/15</h2>
				</div>
				<div class="col-auto  ">
					<a class="btn " href="#"> Delete Contract Period</a>
				</div>

			</div>

			<!--Lane & COMMODITY PAIR TABLE-->
			<div class="row ">
				<div class="col-12 ">
					<table class="table table-responsive table-bordered " >
					
						<thead>
							<tr>
								<th><input type="checkbox" class=""></th>
								<th>Controls</th>
								<th>Origin</th>
								<th>Destination</th>
								<th>Commodity</th>
								<th class="text-nowrap">Rating Unit(PHP)</th>
								<th>50 FT</th>
								<th>20 FT</th>
								<th>6 W</th>
								<th>10 W</th>
								<th>Bids</th>
							</tr>
						</thead>	
						
						<tbody>
							<!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td rowspan="2"><input type="checkbox" class=""></td>
								<td rowspan="2"><i class="material-icons">delete</i></td>
								<td rowspan="2">Cebu City, Cebu, 6000, Philippines</td>
								<td rowspan="2">Mandaue City, Cebu, 6000, Philippines</td>
								<td rowspan="2">Seafood</td>
								<td>Weight (Kg)</td>
								<td class="amount"><?php echo mt_rand(100,499); ?> </td>
								<td class="amount"><?php echo mt_rand(100,499); ?> </td>
								<td class="amount"><?php echo mt_rand(100,499); ?> </td>
								<td class="amount"><?php echo mt_rand(100,499); ?> </td>
								<td rowspan="2">Awarded to <strong>Sugbu Shipping Ltd </strong></td>
								
							</tr>
							<tr>
								<td>Volume (cm3)</td>
								<td class="amount">0.00</td>
								<td class="amount">0.00</td>
								<td class="amount">0.00</td>
								<td class="amount">0.00</td>
							</tr>
							<!--Lane & COMMODITY PAIR END-->
							<!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td rowspan="2"><input type="checkbox" class=""></td>
								<td rowspan="2"><i class="material-icons">delete</i></td>
								<td rowspan="2">献县河街镇万村医院</td>
								<td rowspan="2">伦教鸡洲路段3号（远望大厦4楼）</td>
								<td rowspan="2">Sugarcane</td>
								<td>Weight (Kg)</td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td rowspan="2">11 Bids</td>
								
							</tr>
							<tr>
								<td>Volume (cm3)</td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
							</tr>
							<!--Lane & COMMODITY PAIR END-->
							<!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td rowspan="2"><input type="checkbox" class=""></td>
								<td rowspan="2"><i class="material-icons">delete</i></td>
								<td rowspan="2">Flat/Rm 1002 10/F Ricky Ctr 36 Chong Yip St Kl</td>
								<td rowspan="2">Room 4054/F. Far East Consortium Building204-206 Nathan Road</td>
								<td rowspan="2">Seafood</td>
								<td>Weight (Kg)</td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td rowspan="2">11 Bids</td>
								
							</tr>
							<tr>
								<td>Volume (cm3)</td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
								<td class="amount"><?php echo mt_rand(100,499); ?> - <?php echo mt_rand(500,999); ?></td>
							</tr>
							<!--Lane & COMMODITY PAIR END-->
						</tbody>
						
					</table>
				</div>
			</div>

			
			<!--Lane & COMMODITY CONTROLS-->
			<div class="row op-box-footer">

				<div class="col-auto mr-auto">
					<a class="btn btn-outline-danger " href="#">Withdraw Tender for Selected Lanes</a>
				</div>
				
			</div>
			
		</article>	
	</div>
</section>

<!--OP-BOX BEGIN-->
<section class="row ">
	<div class="col">
		<article class="container-fluid op-box">
		
			<!--HEADER-->
			<div class="row op-box-header align-items-center">
				<div class="col-auto">
					<input type="checkbox" class="">
				</div>
				<div class="col-auto mr-auto">
					<h1>2017/09/15 — 2018/02/15</h2>
				</div>
				<div class="col-auto  ">
					<a class="btn " href="#"> Delete Contract Period</a>
				</div>

			</div>

			<!--Lane & COMMODITY PAIR TABLE-->
			<div class="row ">
				<div class="col-12 ">
					<table class="table table-responsive table-bordered " >
					
						<thead>
							<tr>
								<th><input type="checkbox" class=""></th>
								<th>Controls</th>
								<th>Origin</th>
								<th>Destination</th>
								<th>Commodity</th>
								<th class="text-nowrap">Rating Unit(PHP)</th>
								<th>50 FT</th>
								<th>20 FT</th>
								<th>6 W</th>
								<th>10 W</th>
								<th>Bids</th>
							</tr>
						</thead>	
						
						<tbody>
							<!--Lane & COMMODITY PAIR BEGIN-->
							<tr>
								<td rowspan="2"><input type="checkbox" class=""></td>
								<td rowspan="2"><i class="material-icons">delete</i></td>
								<td rowspan="2">Cebu City, Cebu, 6000, Philippines</td>
								<td rowspan="2">Mandaue City, Cebu, 6000, Philippines</td>
								<td rowspan="2">Seafood</td>
								<td>Weight (Kg)</td>
								<td class="amount"><?php echo mt_rand(100,499); ?> </td>
								<td class="amount"><?php echo mt_rand(100,499); ?> </td>
								<td class="amount"><?php echo mt_rand(100,499); ?> </td>
								<td class="amount"><?php echo mt_rand(100,499); ?> </td>
								<td rowspan="2">Awarded to <strong>Sugbu Shipping Ltd </strong></td>
								
							</tr>
							<tr>
								<td>Volume (cm3)</td>
								<td class="amount">0.00</td>
								<td class="amount">0.00</td>
								<td class="amount">0.00</td>
								<td class="amount">0.00</td>
							</tr>
							<!--Lane & COMMODITY PAIR END-->
							
						</tbody>
						
					</table>
				</div>
			</div>

			
			<!--Lane & COMMODITY CONTROLS-->
			<div class="row op-box-footer">

				<div class="col-auto mr-auto">
					<a class="btn btn-outline-danger " href="#">Withdraw Tender for Selected Lanes</a>
				</div>
				
			</div>
			
		</article>	
	</div>
</section>

<section class="row">
	<div class="col-auto ml-auto">
		<p><a class="btn btn-primary">Add New Contract Period</a></p>
	</div>
</section>
<!--Contract Period END-->



</div>

<?php include("inc.footer.php"); ?>

