<script >
	$(".toggle").click(function(e){
	$(this).parents(".toggleable").find(".tago").slideToggle('fast');
	});
</script>

<script >
	//turn to inline mode
$(document).ready(function() {
	$.fn.editable.defaults.mode = 'inline';
    $('.x-editable.text').editable();
    $('.x-editable.date').editable({
        format: 'YYYY/MM/DD',    
        template: 'YYYY / MMMM / D',    
        combodate: {
                minYear: 2017,
                maxYear: 2067,
                minuteStep: 1
           }
        }); 
    });

</script>

</body>
</html>